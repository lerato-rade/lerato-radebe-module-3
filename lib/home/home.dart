import 'package:app/pages/home.dart';
import 'package:app/profile/profile.dart';
import 'package:flutter/material.dart';

class MyHome extends StatelessWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: NestedScrollView(
            headerSliverBuilder: (context, value) {
              return [
                const SliverAppBar(
                  automaticallyImplyLeading: false,
                  iconTheme: IconThemeData(color: Colors.black),
                  // automaticallyImplyLeading: false,
                  pinned: true,
                  backgroundColor: Color.fromARGB(255, 135, 207, 235),

                  title: Text(
                    "Dashboard",
                    style: TextStyle(color: Colors.black),
                  ),

                  bottom: TabBar(
                    indicatorColor: Color.fromARGB(255, 135, 207, 235),
                    tabs: [
                      Text(
                        "Home",
                        style: TextStyle(color: Colors.black),
                      ),
                      Text(
                        "Profile",
                        style: TextStyle(color: Colors.black),
                      ),
                    ],
                  ),
                )
              ];
            },
            body: const TabBarView(
              children: [Home(), Profile()],
            )));
  }
}
