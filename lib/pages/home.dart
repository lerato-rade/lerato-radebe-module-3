import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: ElevatedButton(onPressed: (() => Navigator.pushNamed(context, "/about")), child: Text("about")),
      body: const Center(
          child: Text(
        "Radebe Holdings",
        style: TextStyle(color: Color.fromARGB(255, 135, 207, 235)),
      )),
    );
  }
}
